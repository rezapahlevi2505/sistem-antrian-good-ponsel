/*
 Navicat Premium Data Transfer

 Source Server         : Server Lokal
 Source Server Type    : MySQL
 Source Server Version : 100422 (10.4.22-MariaDB)
 Source Host           : localhost:3306
 Source Schema         : antrian_good_ponsel

 Target Server Type    : MySQL
 Target Server Version : 100422 (10.4.22-MariaDB)
 File Encoding         : 65001

 Date: 17/03/2024 13:51:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_admin
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin`;
CREATE TABLE `tb_admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `akses_level` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_admin
-- ----------------------------
INSERT INTO `tb_admin` VALUES (47, 'reza', '5f4dcc3b5aa765d61d8327deb882cf99', 'reza@example.com', 'Reza Pahlevi', 1);
INSERT INTO `tb_admin` VALUES (48, 'customer', '5f4dcc3b5aa765d61d8327deb882cf99', 'customer@example.com', 'Customer', 0);

-- ----------------------------
-- Table structure for tb_antrian
-- ----------------------------
DROP TABLE IF EXISTS `tb_antrian`;
CREATE TABLE `tb_antrian`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nomor` int NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `status` enum('belum_dipanggil','sudah_dipanggil','kadaluarsa','aktif','lewat') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_label` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_loket` int NULL DEFAULT NULL,
  `id_jasa` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15708 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_antrian
-- ----------------------------

-- ----------------------------
-- Table structure for tb_jasa
-- ----------------------------
DROP TABLE IF EXISTS `tb_jasa`;
CREATE TABLE `tb_jasa`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_jasa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_jasa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `color` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `kode_jasa`(`kode_jasa` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_jasa
-- ----------------------------
INSERT INTO `tb_jasa` VALUES (1, 'Services', 'A', 'Jasa Services HP', '#49fd8e');

-- ----------------------------
-- Table structure for tb_loket
-- ----------------------------
DROP TABLE IF EXISTS `tb_loket`;
CREATE TABLE `tb_loket`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_loket` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_jasa` int NULL DEFAULT NULL,
  `nama_file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_loket
-- ----------------------------
INSERT INTO `tb_loket` VALUES (1, 'Loket Services A', 1, 'sim');
INSERT INTO `tb_loket` VALUES (2, 'Loket Services B', 1, 'sim');
INSERT INTO `tb_loket` VALUES (3, 'Loket Services C', 1, 'sim');

SET FOREIGN_KEY_CHECKS = 1;
