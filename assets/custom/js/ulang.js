$(document).ready(function () {
    nomor();
    cetak();
    panggil();

});

function nomor() {
    var url = $('#number_url').val();

    $('#selUser').select2({
        minimumInputLength: 0,
        allowClear: true,
        placeholder: 'Pilih Nomor',
        ajax: {
            dataType: 'json',
            url: url,
            delay: 800,
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (data, page) {
                return {
                    results: data
                };
            },
        }
    });
}

function cetak() {
    $('#cetak').on('click', function () {
        var url_cetak = $('#url-cetak').val();
        var data = $('#selUser').val();
        data = JSON.parse(data);
        if (data){
            var id = data.id;
            var cetak = window.open(url_cetak + '/' + id, '_blank');
            cetak.print();
        } else {
            swal.fire({
                position: 'center',
                type: 'warning',
                title: 'Silahkan Pilih Nomor Yang Ingin Anda Cetak',
            });
        }
    });
}

function panggil() {
    $('#panggil').on('click', function () {
        var data = $('#selUser').val();
        data = JSON.parse(data);
        var nomor = $('#selUser').select2('data');
        if (nomor['length'] > 0) {
            nomor = data.nomor;
        }
        nomor = parseInt(nomor);

        if (nomor) {
            play();
        } else {
            swal.fire({
                position: 'center',
                type: 'warning',
                title: 'Silahkan Pilih Nomor Yang Ingin Anda Panggil',
            });
        }
    });
}

function play() {
    // var url_audio = $('#url-audio').val();
    //
    var data = $('#selUser').val();
    data = JSON.parse(data);
    var nomor = $('#selUser').select2('data');
    if (nomor['length'] > 0) {
        nomor = data.nomor;
    }

    nomor = parseInt(nomor);
    //
    // console.log(nomor)
    // var audio = url_audio + '/audio' + nomor + '.mp3';
    // $('audio #panggilan').attr('src', audio);
    if (nomor > 0) {
        play_testing()
    } else {
        swal.fire({
            position: 'center',
            type: 'warning',
            title: 'Audio Tidak Ditemukan',
        });
    }
}

function play_testing() {
    var data = $('#selUser').val();
    var loket = $('#loket').val();
    data = JSON.parse(data);
    loket = JSON.parse(loket);
    var nomor = $('#selUser').select2('data');
    if (nomor['length'] > 0) {
        nomor = data.nomor;
    }
    var number = nomor,
        output = [],
        sNumber = number.toString();
    var length = sNumber.length;
    var url_audio = $('#url-audio').val();

    var kode = url_audio + '/kode/' + loket.kode_jasa + '.mp3';
    var loket = url_audio + '/loket/' + loket.nama_file + '.mp3';
    var opening = url_audio + '/opening.mp3';
    var ending = url_audio + '/ending.mp3';
    var aud = document.getElementById("suara-panggilan");

    for (var i = 0, len = sNumber.length; i < len; i += 1) {
        output.push(+sNumber.charAt(i));
    }

    if (length === 3) {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                var url_pertama = url_audio + '/ratusan/' + pertama + '.mp3';
                $('audio #panggilan').attr('src', url_pertama);
                $('audio').get(0).load();
                $('audio').get(0).play();
                aud.onended = function () {
                    var kedua = output[1];
                    if (kedua < 1) {
                        var ketiga = output[2];
                        if (ketiga > 0) {
                            var url_ketiga = url_audio + '/satuan/' + ketiga + '.mp3';
                            $('audio #panggilan').attr('src', url_ketiga);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', ending);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        } else {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        }
                    } else if (kedua > 1) {
                        var url_kedua = url_audio + '/puluhan/' + kedua + '.mp3';
                        $('audio #panggilan').attr('src', url_kedua);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            var ketiga = output[2];
                            var url_ketiga = url_audio + '/satuan/' + ketiga + '.mp3';
                            $('audio #panggilan').attr('src', url_ketiga);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', ending);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio #panggilan').attr('src', loket);
                                    $('audio').get(0).load();
                                    $('audio').get(0).play();
                                    aud.onended = function () {
                                        $('audio').get(0).pause();
                                    };
                                };
                            };
                        };
                    } else {
                        var belasan = output[2];
                        var url_belasan = url_audio + '/belasan/' + belasan + '.mp3';
                        if (belasan == 0){
                            url_belasan = url_audio + '/puluhan/' + output[0] + '.mp3';
                        }
                        $('audio #panggilan').attr('src', url_belasan);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        };
                    }
                };
            };
        };
    } else if (length === 2) {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                if (pertama > 1) {
                    var url_pertama = url_audio + '/puluhan/' + pertama + '.mp3';
                    $('audio #panggilan').attr('src', url_pertama);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        var kedua = output[1];
                        var url_kedua = url_audio + '/satuan/' + kedua + '.mp3';
                        $('audio #panggilan').attr('src', url_kedua);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', ending);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio #panggilan').attr('src', loket);
                                $('audio').get(0).load();
                                $('audio').get(0).play();
                                aud.onended = function () {
                                    $('audio').get(0).pause();
                                };
                            };
                        };
                    };
                } else {
                    var belasan = output[1];
                    var url_belasan = url_audio + '/belasan/' + belasan + '.mp3';
                    if (belasan == 0){
                        url_belasan = url_audio + '/puluhan/' + output[0] + '.mp3';
                    }
                    $('audio #panggilan').attr('src', url_belasan);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        $('audio #panggilan').attr('src', ending);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio #panggilan').attr('src', loket);
                            $('audio').get(0).load();
                            $('audio').get(0).play();
                            aud.onended = function () {
                                $('audio').get(0).pause();
                            };
                        };
                    };
                }
            };
        };
    } else {
        $('audio #panggilan').attr('src', opening);
        $('audio').get(0).load();
        $('audio').get(0).play();

        aud.onended = function () {
            $('audio #panggilan').attr('src', kode);
            $('audio').get(0).load();
            $('audio').get(0).play();
            aud.onended = function () {
                var pertama = output[0];
                var url_pertama = url_audio + '/satuan/' + pertama + '.mp3';
                $('audio #panggilan').attr('src', url_pertama);
                $('audio').get(0).load();
                $('audio').get(0).play();
                aud.onended = function () {
                    $('audio #panggilan').attr('src', ending);
                    $('audio').get(0).load();
                    $('audio').get(0).play();
                    aud.onended = function () {
                        $('audio #panggilan').attr('src', loket);
                        $('audio').get(0).load();
                        $('audio').get(0).play();
                        aud.onended = function () {
                            $('audio').get(0).pause();
                        };
                    };
                };
            };
        };
    }
}
