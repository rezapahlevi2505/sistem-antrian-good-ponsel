$(document).ready(function () {
    forgot();
    batal();
    submit();
});

function forgot(){
    $('#forgot').on('click', function () {
        $('#form-login').fadeOut('fast').hide();
        $('#form-forgot').fadeIn('fast').show();
    });
}
function batal(){
    $('#batal').on('click', function () {
        $('#form-forgot').fadeOut('fast').hide();
        $('#form-login').fadeIn('fast').show();
    });
}
function submit(){

    $('.form-login').submit(function (e) {
        e.preventDefault();
        $('#loader').show().fadeIn('fast');
        $('button').attr('disabled', true);

        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (json) {
                $('#loader').fadeOut('fast').addClass('hide');
                $('button').attr('disabled', false);
                var data = JSON.parse(json);
                $('.form-group').removeClass('validated');
                $('.invalid-feedback').remove();
                $('.form-control').removeClass('is-invalid');
                if (data.status === 'error') {
                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: data.message,
                    });

                    $.each(data.errors, function (field, message) {
                        if (message) {
                            $('[name=' + field + ']').parents('.form-group').addClass('validated');
                            $('[name=' + field + ']').after('<div class="invalid-feedback">' + message + '</div>');
                            $('[name=' + field + ']').addClass('is-invalid');
                        }
                    });
                } else if (data.status == 'success') {
                    if (data.message){
                        if (data.url){
                            swal.fire({
                                title: data.message,
                                type: 'success',
                                confirmButtonText: 'Done',
                                reverseButtons: true
                            }).then(function (result) {
                                window.location.replace(data.url);
                            });
                        }else{
                            swal.fire({
                                title: data.message,
                                type: 'success',
                                confirmButtonText: 'Done',
                                reverseButtons: true
                            }).then(function (result) {
                                window.location.reload();
                            });
                        }

                    }else {
                        window.location.reload();

                    }
                } else {

                    swal.fire({
                        position: 'center',
                        type: 'warning',
                        title: 'Ada kesalahan',
                    });
                }
            }

        });


        console.log('hide')
        return false;
    });

}

