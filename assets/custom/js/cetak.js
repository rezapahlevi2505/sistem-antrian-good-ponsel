$(document).ready(function () {
    lastPoll();
    add();
    doPoll();
    reset();

});

function add() {
    $('#add').on('click', function () {
        var url = $('#add_url').val();
        var url_api = $('#add_url_api').val();
        var url_cetak = $('#url-cetak').val();
        var id_jasa = $('#id-jasa').val();
        var data = {'id_jasa' : id_jasa};

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            encode: 'true',
            success: function (data) {
                $('#nomor_antrian_terakhir').html(data.nomor.nomor_label);
                $('#id_antrian_terakhir').val(data.nomor.id);
                $('#value_antrian_terakhir').val(JSON.stringify(data.nomor));
                console.log('add sukses');
                var cetak = window.open(url_cetak + '/' + data.nomor.id, '_blank');
                cetak.print();

            },
            error: function (data) {
                console.log('gagal');
            }
        });
    });
}

function reset() {
    $('#reset').on('click', function () {
        var url = $('#url-reset').val();
        var data = {};

        swal.fire({
            title: 'Anda yakin ?',
            text: "Ingin menghapus data antrian hari ini",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    encode: 'true',
                    success: function (data) {
                        doPoll();
                        lastPoll();
                    },
                    error: function (data) {
                        console.log('gagal');
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal.fire(
                    'Batal',
                    'data tidak di hapus',
                    'error'
                );
            }
        });


    });
}

function lastPoll() {
    var url = $('#url-last').val();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        encode: 'true',
        success: function (data) {
            $('#nomor_antrian_terakhir').html(data.nomor.nomor_label);
            $('#id_antrian_terakhir').val(data.nomor.id);
            $('#value_antrian_terakhir').val(JSON.stringify(data.nomor));
            console.log(data.nomor.nomor);
        },
        error: function (data) {
            console.log('gagal');
        }
    })
    setTimeout(lastPoll, 5000);
}

function doPoll() {
    var url = $('#url').val();
    var id = $('#id_antrian').val();
    var nomor = $('#nomor_antrian').html();
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        encode: 'true',
        success: function (data) {
            $('#nomor_antrian').html(data.nomor.nomor_label);
            $('#id_antrian').val(data.nomor.id);
            $('#value_antrian').val(JSON.stringify(data.nomor));
        },
        error: function (data) {
            console.log('gagal');
        }
    });
    setTimeout(doPoll, 5000);
}