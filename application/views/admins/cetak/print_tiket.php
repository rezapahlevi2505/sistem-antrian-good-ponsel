<!DOCTYPE html>

<html lang="en">


<head>

    <meta charset="utf-8">

    <title>Receipt</title>


    <!-- Normalize or reset CSS with your favorite library -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">


    <!-- Load paper.css for happy printing -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">


    <!-- Set page size here: A5, A4 or A3 -->

    <!-- Set also "landscape" if you need -->

    <style>

        @page {
            size: 58mm;
            margin: 0 0 0 0;
        }

        /* output size */

        body.receipt .sheet {
            width: 58mm;
            padding: 0px
        }

        /* sheet size */

        @media print {
            body.receipt .sheet {
                width: 58mm;
                padding: 0px;
                margin: 0 0 0 0;
            }

            @page {
                margin: 0 0 0 0
            }
        }

        /* fix for Chrome */

        p {
            font-size: 13px;
            margin: 0px;
            font-family: Tahoma, Verdana, Segoe, sans-serif;
        }


        @media screen {

            body {
                background: #e0e0e0
            }

        }

    </style>

</head>


<body class="receipt" onload="window.print()">

<section class="sheet" style="padding: 3mm 3mm 3mm 3mm">


    <?php $str = base_url();
    $str = preg_replace('#^https?://#', '', rtrim($str, '/')); ?>
    <table width="100%">
        <tr>
            <td style="text-align: center; font-size: 30px; font-family: Tahoma, sans-serif; font-weight: bold">
                SELAMAT DATANG
            </td>
        </tr>
        <tr>

            <td style="text-align: center; font-size: 12px; font-family: Tahoma, sans-serif; font-weight: bold">
                Good Ponsel
            </td>
        </tr>
    </table>
    <hr>
    <table width="100%" style="margin: 0; padding: 0">
        <tbody>
        <tr>
            <td style="text-align: left; font-size: 10px; font-family: Tahoma, sans-serif; font-weight: bold; text-align: center;"><?= date('Y-m-d') ?></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"
                style="text-align: center; font-size:25px; font-family: Tahoma, sans-serif; font-weight: bold">
                ANTRIAN
            </td>
        </tr>
        <tr style="padding-bottom: 1em;">
            <td colspan="3"
                style="text-align: center; font-size: 70px; font-family: Tahoma, sans-serif; font-weight: bold;">
                <?= $antrian->nomor_label ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center;">
                <img src="<?php echo base_url() . 'assets/good-ponsel/https.png' ?>" width="100px" style="padding-top: 0">
            </td>
        </tr>
        <tr>

            <td colspan="3"
                style="text-align: center; font-size: 17px; font-family: Tahoma, sans-serif; font-weight: normal;">
                <b><?= $antrian->nama_loket ?></b> 
            </td>
        </tr>
        </tbody>
    </table>

    <hr>
    <table width="100%">
        <tbody>
        <tr>
            <td style="text-align: center; font-size: 15px; font-family: Tahoma, sans-serif; font-weight: normal">
                Jalan Surapati No. 122 Singaraja, Buleleng - Bali
                <!--            <br>-->
                <!--            Call Center (+62362 25471)-->
            </td>
        </tr>
        </tbody>
    </table>


</section>
<script>
    window.print();
    window.onafterprint = window.close;
</script>
</body>

</html>
