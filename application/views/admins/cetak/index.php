<input type="hidden" value="<?= base_url('next') ?>" id="next_url">
<input type="hidden" value="<?= base_url('skip') ?>" id="skip_url">
<input type="hidden" value="<?= base_url('add') ?>" id="add_url">
<input type="hidden" value="<?= base_url('post-antrian') ?>" id="add_url_api">
<input type="hidden" value="<?= json_encode($loket) ?>" id="loket">
<input type="hidden" value="<?= $loket->id_jasa ?>" id="id-jasa">
<input type="hidden" id="url" value="<?= base_url('/now') ?>">
<input type="hidden" id="url-last" value="<?= base_url('/last') ?>">
<input type="hidden" id="url-cetak" value="<?= base_url('/proweb/cetak/print') ?>">
<input type="hidden" id="url-reset" value="<?= base_url('/proweb/cetak/reset') ?>">
<div class="row">
    <div class="col-lg-6">
        <input type="hidden" value="<?= $terakhir->id ?>" id="id_antrian_terakhir">
        <input type="hidden" value="<?= $terakhir ?>" id="value_antrian_terakhir">
        <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center">
            <div class="inner">
                <h3 id="nomor_antrian_terakhir" class="nomor-antrian margin-atas"><?= $terakhir->nomor_label ?></h3>
                <p class="margin-atas">Nomor Antrian Terakhir</p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <input type="hidden" value="<?= $antrian->id ?>" id="id_antrian">
        <input type="hidden" value="<?= $antrian ?>" id="value_antrian">
        <div class="kt-portlet kt-portlet--height-fluid  text-mobile" style="text-align: center;">
            <div class="inner">
                <h3 id="nomor_antrian" class="nomor-antrian margin-atas"><?= $antrian->nomor_label ?></h3>
                <p class="margin-atas">Nomor Antrian Saat Ini</p>
            </div>
        </div>
    </div>
</div>
<?php if ($akses_level == 1) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-sm  mobile-row">
                    <button id="add" class="btn btn-success custom-block">Cetak Nomor Baru</button>
                </div>
                <div class="col-sm">
                    <button id="reset" class="btn btn-warning custom-block">Reset</button>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-sm  mobile-row">
                    <button id="add" class="btn btn-success custom-block">Cetak Nomor Baru</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
