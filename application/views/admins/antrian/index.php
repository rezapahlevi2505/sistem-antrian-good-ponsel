<input type="hidden" value="<?= base_url('next') ?>" id="next_url">
<input type="hidden" value="<?= base_url('skip') ?>" id="skip_url">
<input type="hidden" value="<?= base_url('add') ?>" id="add_url">
<input type="hidden" value='<?= json_encode($jasa) ?>' id="loket">
<input type="hidden" id="url" value="<?= base_url('/now') ?>">
<input type="hidden" id="url-last" value="<?= base_url('/last') ?>">
<input type="hidden" id="url-audio" value="<?= base_url() ?>/assets/good-ponsel/audio-panggilan">
<audio controls style="display: none" id="suara-panggilan">
    <source id="panggilan" src="" type="audio/mp3">
</audio>
<div class="row">
    <div class="col-lg-6">
        <input type="hidden" value="<?= $antrian->id ?>" id="id_antrian">
        <input type="hidden" value="<?= $antrian->nomor_label ?>" id="value_antrian">
        <input type="hidden" value="<?= $antrian->status ?>" id="status_antrian">
        <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center">
            <div class="inner">
                <h3 id="nomor_antrian" class="nomor-antrian margin-atas"><?= $antrian->nomor_label ?></h3>
                <p class="margin-atas">Nomor Antrian Saat Ini</p>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <input type="hidden" value="<?= $terakhir->id ?>" id="id_antrian_terakhir">
        <input type="hidden" value="<?= $terakhir->nomor_label ?>" id="value_antrian_terakhir">
        <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center">
            <div class="inner">
                <h3 id="nomor_antrian_terakhir" class="nomor-antrian margin-atas"><?= $terakhir->nomor_label ?></h3>
                <p class="margin-atas">Nomor Antrian Terakhir</p>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-2">
    </div>
    <div class="col-lg-8">
        <div class="row">
            <div class="col-sm  mobile-row">
                <button id="next" class="btn btn-success custom-block">Next</button>
            </div>
            <div class="col-sm">
                <button id="skip" class="btn btn-warning custom-block">Skip</button>
            </div>
            <div class="col-sm">
                <button id="panggil" class="btn btn-danger custom-block">Panggil</button>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
    </div>

</div>