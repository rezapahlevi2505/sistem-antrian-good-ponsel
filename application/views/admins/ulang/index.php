<input type="hidden" value="<?= base_url('semua') ?>" id="number_url">
<input type="hidden" id="url-cetak" value="<?= base_url('/proweb/cetak/print') ?>">
<input type="hidden" value='<?= json_encode($jasa) ?>' id="loket">
<input type="hidden" id="url-audio" value="<?= base_url() ?>/assets/good-ponsel/audio-panggilan">
<audio controls style="display: none" id="suara-panggilan">
    <source id="panggilan" src="" type="audio/mp3">
</audio>
<div class="row">
    <div class="col-lg-2">
    </div>
    <div class="col-lg-8">
        <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center">
            <div class="inner">
                <p class="margin-atas" >Nomor Antrian Saat Ini</p>
                <select id='selUser' style='width: 90%;'>
                </select>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="row">
    <div class="col-lg-2">
    </div>
    <div class="col-lg-8">
        <div class="row">
            <div class="col-sm  mobile-row">
                <button id="cetak" class="btn btn-success custom-block">Cetak</button>
            </div>
            <div class="col-sm">
                <button id="panggil" class="btn btn-danger custom-block">Panggil</button>
            </div>
        </div>
    </div>
    <div class="col-lg-2">
    </div>

</div>