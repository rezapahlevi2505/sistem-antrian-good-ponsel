<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
			<span class="kt-portlet__head-icon">
				<i class="flaticon-avatar"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Jasa
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <btn class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#modal-create">
                        <i class="la la-plus"></i>
                        Add Jasa
                    </btn>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body" style="overflow-x: scroll">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable datatable">
            <thead>
            <tr>
                <th class="d-none"></th>
                <th width="20">No</th>
                <th>Nama Loket</th>
                <th>Nama Jasa</th>
                <th width="130">Option</th>
            </tr>
            </thead>
            <?php $no = 1 ?>
            <tbody>
            <?php foreach ($loket as $datas) : ?>
                <tr>
                    <td class="d-none data-row">
                        <textarea><?php echo json_encode($datas) ?></textarea>
                    </td>
                    <td><?php echo $no ?></td>
                    <td><?php echo $datas->nama_loket ?></td>
                    <td><?php echo $datas->nama_jasa ?></td>
                    <td>
                        <a href="#"
                           class="btn btn-success btn-elevate btn-elevate-air btn-edit">Edit</a>
                        <a href="#"
                           data-action="<?php echo base_url() ?>proweb/loket/delete/<?php echo $datas->id ?>"
                           class="btn btn-danger btn-elevate btn-elevate-air btn-delete">Delete</a>
                    </td>
                </tr>
                <?php $no++ ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
<!--begin::Modal-->

<form method="post" action="<?php echo base_url() . 'proweb/loket/createprocess'; ?>" class="form-send"
      autocomplete="off">
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Jasa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Loket</label>
                        <input type="text" class="form-control" placeholder="Nama Loket" name="nama_loket">
                    </div>
                    <div class="form-group">
                        <label>Jasa</label>
                        <select class="form-control select2" style="width: 100%" name="id_jasa">
                            <option value="">Pilih Jasa</option>
                            <?php foreach ($jasa as $j) { ?>
                                <option value="<?= $j->id ?>"><?= $j->nama_jasa ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama File</label>
                        <input type="text" class="form-control" placeholder="Nama File" name="nama_file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </div>
</form>

<form method="post" action="<?php echo base_url() . 'proweb/loket/update'; ?>" class="form-send" autocomplete="off">
    <div class="modal" id="modal-edit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Jasa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <input type="hidden" class="form-control" name="id">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Loket</label>
                        <input type="text" class="form-control" placeholder="Nama Loket" name="nama_loket">
                    </div>
                    <div class="form-group">
                        <label>Jasa</label>
                        <select class="form-control select2" style="width: 100%" name="id_jasa">
                            <option value="">Pilih Jasa</option>
                            <?php foreach ($jasa as $j) { ?>
                                <option value="<?= $j->id ?>"><?= $j->nama_jasa ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nama File</label>
                        <input type="text" class="form-control" placeholder="Nama File" name="nama_file">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary" name="submit" value="Save">
                </div>
            </div>
        </div>
    </div>
</form>


