<nav id="ht-main-nav"><a href="#" class="ht-nav-toggle active"><span></span></a>
    <div class="container">
        <div class="row">
            <div class="col-md-12"><img class="img-fluid side-logo mb-3"
                                        src="<?php echo base_url() ?>assets/template_front/images/redsystem-logo.png"
                                        alt="Red System Logo" title="Red System Logo" width="234" height="25">
                <p class="mb-5" align="justify">Merupakan software-house dengan pengalaman handling sistem
                    multinasional dengan preoritas Kecepatan, Efisiensi, dan Desain Berkelas.</p>
                <div class="form-info"><h4 class="title">Contact info</h4>
                    <ul class="contact-info list-unstyled mt-4">
                        <li class="mb-4"><i class="flaticon-location"></i><span>Alamat:</span>
                            <p>Jl. Ratna No 68 G, Tonja, Denpasar Utara, Denpasar - Bali . 80239</p></li>
                        <li class="mb-4"><i class="flaticon-call"></i><span>Phone:</span><a
                                    href="tel:(0361)4746102">(0361)4746102</a></li>
                        <li><i class="flaticon-email"></i><span>Email</span><a href="mailto:info@redsystem.id">info@redsystem.id</a>
                        </li>
                    </ul>
                </div>
                <div class="social-icons social-colored mt-5">
                    <ul class="list-inline">
                        <li class="mb-2 social-facebook"><a
                                    href="https://www.facebook.com/Red-System-444865549587464/" title="Red System"
                                    target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="mb-2 social-instagram"><a href="https://www.instagram.com/red.system/"
                                                             title="Red System" target="_blank"><i
                                        class="fab fa-instagram"></i></a></li>
                        <li class="mb-2 social-twitter"><a href="https://www.twitter.com/forstaff.id"
                                                           title="Red System" target="_blank"><i
                                        class="fab fa-twitter"></i></a></li>
                        <li class="mb-2 social-linkedin"><a href="https://www.linkedin.com/company/redsystemid/"
                                                            title="Red System" target="_blank"><i
                                        class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<section class="fullscreen-banner p-0 banner o-hidden"
         data-bg-img="<?php echo base_url() ?>assets/template_front/images/pattern/01.png">
    <div class="align-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 order-lg-12">
                    <div class="mouse-parallax">
                        <div class="bnr-img1 animated fadeInRight delay-4 duration-4"><img
                                    class="img-center rotateme"
                                    src="<?php echo base_url() ?>assets/template_front/images/banner/latar-belakang-perangkat-laptop-red-system.png"
                                    alt="Backgroud Image" title="Backgroud Image" width="100%" height="100%"></div>
                        <img class="img-center bnr-img2 animated zoomIn delay-5 duration-4"
                             src="<?php echo base_url() ?>assets/template_front/images/banner/<?php echo $page->thumbnail ?>"
                             alt="<?php echo $page->thumbnail_alt ?>"
                             title="<?php echo $page->thumbnail_alt ?>" width="439" height="373"></div>
                </div>
                <div class="col-lg-7 col-md-12 order-lg-1 md-mt-5"><h4
                            class="mb-4 animated bounceInLeft delay-2 duration-4 font-w-5"
                            style="text-transform: none; margin-bottom: 0px !important;"><?php echo $page->title_sub ?></h4>
                    <h1 class="bounceInRight delay-3 duration-4" style="margin-bottom: 0px !important;">
                        <span><?php echo $page->title ?></span>
                    </h1>
                    <p class="lead animated fadeInUp delay-4 duration-4" style="margin-bottom: 30px !important;">
                        <?php echo $page->description ?></p>
                    <div class="d-flex align-items-center animated fadeInUp delay-4 duration-5"><a
                                class="btn btn-theme" id="button"><span style="color: white">Mulai <i
                                        class="fas fa-long-arrow-alt-right"></i></span></a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="page-content">
    <section class="pos-r" id="myDiv">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 image-column bg-contain bg-pos-l"
                     data-bg-img="<?php echo base_url() ?>assets/template_front/images/pattern/07.png">
                    <div class="round-animation"><img class="img-fluid"
                                                      src="<?php echo base_url() ?>assets/template_front/images/about/<?php echo $welcome->thumbnail ?>"
                                                      alt="<?php echo $welcome->thumbnail_alt ?>"
                                                      title="<?php echo $welcome->thumbnail_alt ?>" width="998"
                                                      height="960"></div>
                </div>
                <div class="col-lg-7 col-md-12 ml-auto md-mt-5">
                    <div class="section-title mb-4"><h6><?php echo $welcome->title_sub ?></h6>
                        <h2 class="title"><?php echo $welcome->title_menu ?></h2></div>
                    <?php echo $welcome->description ?></div>
            </div>
        </div>
    </section>
    <section class="text-center o-hidden">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 ml-auto mr-auto">
                    <div class="section-title"><h6><?php echo $services->title_sub ?></h6>
                        <h2 class="title"><?php echo $services->title_menu ?></h2></div>
                </div>
            </div>
            <div class="row" style="overflow-x: auto">
                <?php foreach ($services_row as $key => $row) { ?>
                    <div class="col-lg-4 col-md-12 md-mt-5">
                        <div class="work-process">
                            <div class="work-process-inner"><img class="img-center margin-bottom"
                                                                 src="<?php echo $this->main->image_preview_url($row->image) ?>"
                                                                 alt="<?php echo $row->images_title ?>"
                                                                 title="<?php echo $row->images_title ?>" width="120"
                                                                 height="120"> <h4><?php echo $row->title ?></h4>
                                <p class="mb-0"><?php echo $row->desc ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="grey-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="counter style-2"><img class="img-center"
                                                      src="<?php echo base_url() ?>assets/template_front/images/counter/project-done-red-system.png"
                                                      alt="Proyek Selesai" title="Proyek Selesai" width="109"
                                                      height="100"> <span class="count-number" data-to="<?php echo $project_count ?>"
                                                                          data-speed="1000"><?php echo $project_count ?></span> <h5>Proyek
                            Selesai</h5></div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 xs-mt-5">
                    <div class="counter style-2"><img class="img-center"
                                                      src="<?php echo base_url() ?>assets/template_front/images/counter/our-client-red-system.png"
                                                      alt="Klien Kami" title="Klien Kami" width="140" height="100">
                        <span class="count-number" data-to="<?php echo $client_count ?>" data-speed="1000"><?php echo $client_count ?></span> <h5>Klien Kami</h5>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 sm-mt-5">
                    <div class="counter style-2"><img class="img-center"
                                                      src="<?php echo base_url() ?>assets/template_front/images/counter/our-partner-red-system.png"
                                                      alt="Partner Kami" title="Partner Kami" width="178"
                                                      height="100"> <span class="count-number" data-to="<?php echo $partner_count ?>"
                                                                          data-speed="1000"><?php echo $partner_count ?></span> <h5>Partner
                            Kami</h5></div>
                </div>
            </div>
        </div>
    </section>
    <section class="animatedBackground"
             data-bg-img="<?php echo base_url() ?>assets/template_front/images/pattern/05.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12">
                    <div class="section-title mb-0"><h6>Testimonial</h6>
                        <h2 class="title">Anda Dapat Melihat umpan balik klien kami</h2> <a class="btn btn-theme"
                                                                                            href="testimonial.html"
                                                                                            id="button"><span
                                    style="color: white">Testimonial lainnya <i class="fas fa-long-arrow-alt-right"></i></span></a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 md-mt-5">
                    <div class="owl-carousel owl-theme" data-items="1">
                        <div class="item">
                            <div class="testimonial">
                                <div class="testimonial-content"><p>Aplikasi manajemen stok dan akunting dari Red
                                        System dapat menyesuaikan dengan alur kerja Kami, sehingga sangat membantu
                                        kinerja staff dalam perusahaan.</p>
                                    <div class="testimonial-caption"><h5>PT. Tamba Waras (Kutus-Kutus)</h5></div>
                                </div>
                                <div class="testimonial-quote"><img class="testimonial--img-center-vertical"
                                                                    src="<?php echo base_url() ?>assets/template_front/images/client/pt-tamba-waras-kutus-kutus-red-system-1904091402.png"
                                                                    alt="PT. Tamba Waras (Kutus-kutus)"
                                                                    title="PT. Tamba Waras (Kutus-kutus)" width="80"
                                                                    height="auto"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial">
                                <div class="testimonial-content"><p>Website yang sesuai dengan permintaan dan
                                        membuat Hogwartz lebih dikenal di media internet.</p>
                                    <div class="testimonial-caption"><h5>Hogwartz The Pub</h5></div>
                                </div>
                                <div class="testimonial-quote"><img class="testimonial--img-center-vertical"
                                                                    src="<?php echo base_url() ?>assets/template_front/images/client/hogwartz-the-pub-red-system-1904091410.png"
                                                                    alt="Hogwartz The Pub" title="Hogwartz The Pub"
                                                                    width="80" height="auto"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial">
                                <div class="testimonial-content"><p>Website yang dibuat sesuai dengan permintaan,
                                        design web yang menarik dan hasilnya sesuai ekspetasi.</p>
                                    <div class="testimonial-caption"><h5>Oris Cake</h5></div>
                                </div>
                                <div class="testimonial-quote"><img class="testimonial--img-center-vertical"
                                                                    src="<?php echo base_url() ?>assets/template_front/images/client/oris-cake-red-system-1904091359.png"
                                                                    alt="Oris Cake" title="Oris Cake" width="80"
                                                                    height="auto"></div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial">
                                <div class="testimonial-content"><p>Sistem Stok dan Akunting yang dibuat oleh Red
                                        System sangat susuai dengan kebutuhan Kami. Maintenance yang cepat dan akurat
                                        membuat kinerja perusahaan Kami jauh lebih bagus dan beban kerja lebih
                                        ringan.</p>
                                    <div class="testimonial-caption"><h5>PT.Angsa Kusuma Indah (Graha Kita 18)</h5>
                                    </div>
                                </div>
                                <div class="testimonial-quote"><img class="testimonial--img-center-vertical"
                                                                    src="<?php echo base_url() ?>assets/template_front/images/client/ptangsa-kusuma-indah-graha-kita-18-red-system-1904091404.png"
                                                                    alt="PT. Angsa Kusuma Indah (Graha Kita 18)"
                                                                    title="PT. Angsa Kusuma Indah (Graha Kita 18)"
                                                                    width="80" height="auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-0">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-8 col-md-12 ml-auto mr-auto">
                    <div class="section-title"><h2 class="title">Klien Kami</h2></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="ht-clients d-flex flex-wrap text-center">
                        <?php foreach ($client as $cl) { ?>
                            <?php if ($cl->link) { ?>
                                <div class="border-clients col-md-3"><a href="#" target="_blank">
                                        <div class="clients-logo"><img class="img-center"
                                           src="<?php echo base_url() ?>assets/template_front/images/client/<?php echo $cl->client_image ?>"
                                           alt="<?php echo $cl->client_company ?>" title="<?php echo $cl->client_company ?>"></div>
                                        <div><p class="color-black"><?php echo $cl->client_company ?></p></div>
                                    </a></div>
                            <?php } else if ($cl->client_company == 'Kopi Box') { ?>

                                <div class="border-clients col-md-3"><a href="https://gofood.link/u/2yZjLy" target="_blank">
                                        <div class="clients-logo"><img class="img-center"
                                           src="<?php echo base_url() ?>assets/template_front/images/testimonial/<?php echo $cl->client_image ?>"
                                           alt="<?php echo $cl->client_company ?>" title="<?php echo $cl->client_company ?>">
                                        </div>
                                        <div><p class="color-black"><?php echo $cl->client_company ?></p></div>
                                    </a></div>
                            <?php } else { ?>
                                <div class="border-clients col-md-3">
                                    <a type="button" class="button-client" onclick="klik()">
                                        <div class="clients-logo"><img class="img-center"
                                           src="<?php echo base_url() ?>assets/template_front/images/client/<?php echo $cl->client_image ?>"
                                           alt="<?php echo $cl->client_company ?>"
                                           title="<?php echo $cl->client_company ?>"></div>
                                        <div><p class="color-black"><?php echo $cl->client_company ?></p></div>
                                    </a></div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>