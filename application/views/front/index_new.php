<html lang="en"
      class="wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n5-active wf-roboto-n6-active wf-roboto-n7-active wf-active">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <title>Sistem Antrian SIM Online</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700"
          media="all">
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <style>
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            padding-top: 30px;
            height: 0;
            overflow: hidden;
        }

        .video-container iframe, .video-container object, .video-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            width: 40%;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

        .container {
            padding: 2px 16px;
        }
    </style>

    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-reboot.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/custom/css/front.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="shortcut icon"
          href="<?= base_url() ?>assets/polres/POLANTAS.png">
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"
      style="">
<nav class="navbar navbar-light none-mobile" style="background-color: #dfdfdf">
    <div class="container col-lg-3" style="width: auto; height: 200px;">
        <img src="<?= base_url() ?>/assets/polres/bali.png" style="width: auto; height: 200px;">
    </div>
    <div class="none-mobile container col-lg-8">
        <a class="navbar-brand center-desktop col-lg-12" href="#" style="color: black; font-size: 85px !important;">ANTOS (Antrian Online SPKT)</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01"
            aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation" >
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample01">
        <ul class="navbar-nav mr-auto" style="text-align: right">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('/proweb') ?>">Login</a>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar navbar-dark bg-dark none-desktop">

    <div style="width: 80%">
        <a class="navbar-brand center-desktop col-lg-12" href="#">ANTOS (Antrian Online SPKT)</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01"
            aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample01">
        <ul class="navbar-nav mr-auto" style="text-align: right">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('/proweb') ?>">Login</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <input type="hidden" id="url" value="<?= base_url('/get-all') ?>">
    <div class="row">
        <div class="col-lg-5" style="background-color: white; min-height: 80vh;">
            <?php foreach ($antrian as $key => $a) { ?>
                <?php if ($key % 2 != 0) { ?>
                    <div class="row" data-key="<?= $key ?>" style="margin-left: 5px; margin-right: 5px">
                <?php } ?>

                <?php if (count($antrian) == $key){ ?>
                <div class="card col-lg-11"
                     style="margin: 10px; background-color: <?= $a->color ?>;">
                <?php }elseif (count($antrian) % 2 != 0){ ?>
                <div class="card col-lg-5"
                     style="margin: 17px; background-color: <?= $a->color ?>;">
            <?php } ?>
                <div class="card-body">
                    <p class="card-title"
                       style="font-size: 100px; text-align: center; line-height: normal"
                       id="nomor-<?= $a->id_jasa ?>"><?= $a->nomor_label ?></p>
                    <p class="card-text"
                       style="font-size: 23px; font-weight: bold; text-align: center; letter-spacing: 2px"><?= $a->nama_loket ?></p>
                </div>
                </div>
                <?php if ($key % 2 == 0){ ?>
                </div>
            <?php } ?>
                <?php if (count($antrian) == $key) { ?>
                    </div>
                <?php } ?>
            <?php } ?>

<!--            <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center;">-->
                <div style="background-color: dimgray; width: 109%; margin-left: -15px; margin-left: -15px; margin-top: 5px">
                    <marquee class="teks-bawah" style="color: white">Pemohon Wajib Hadir 10 Nomor Antrian
                        Sebelumnya | &copy;Red System <?= date('Y') ?>
                    </marquee>
                </div>
<!--            </div>-->
        </div>

        <div class="col-lg-7" style="background-color: dimgray">
            <div class="row">
                <div class="container-fluid none-mobile" style="padding-top: 5px">
                    <div class="video-container">
                        <iframe width="560" height="315"
                                src="https://www.youtube.com/embed/videoseries?list=PLV15jcpqBisv9ucKGrSoyHJwajspDEis8&index=1&loop=1&autoplay=1"
                                frameborder="0"
                                allowfullscreen></iframe>
                    </div>
                </div>
                <div class="container-fluid none-mobile">
                    <div class="container">
                        <p class="lokasi"
                           style="font-family: Roboto; color: white; font-size: 20px; text-align: center">LOKASI</p>
                        <div class="container-fluid col-lg-12">
                            <p class="lokasi"
                               style="font-family: Roboto; color: white; font-size: 20px; text-align: center">Jalan
                                Surapati No.122 Singaraja Buleleng - Bali</p>

                        </div>
                        <p class="telepon"
                           style="font-family: Roboto; color: white; font-size: 20px; text-align: center">
                            TELEPON</p>
                        <div class="container-fluid col-lg-10">
                            <p class="lokasi"
                               style="font-family: Roboto; color: white; font-size: 20px; text-align: center">+62
                                362
                                25471</p>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<footer class="footer-desktop" style=" background-color: #37373a; ">
    <div class="container-fluid full-height">

        <div class="row full-height" style="text-align: center;">
            <div class="container-fluid none-desktop" style="background-color: dimgray;">
                <img src="<?= base_url() ?>/assets/polres/bali.png" style="width: auto; height: 130px;">
                <br>
                <p class="lokasi"
                   style="font-family: Roboto; color: white; font-size: 20px; text-align: center; margin-bottom: 0">
                    LOKASI</p>
                <div class="container-fluid" style="width: 70% !important;">
                    <p class="lokasi"
                       style="font-family: Roboto; color: white; font-size: 15px; text-align: center">
                        Jalan Surapati No.122 Singaraja Buleleng - Bali</p>
                </div>
                <br>
                <p class="telepon"
                   style="font-family: Roboto; color: white; font-size: 20px; text-align: center; margin-bottom: 0">
                    TELEPON</p>
                <div class="container-fluid col-lg-10">
                    <p class="lokasi"
                       style="font-family: Roboto; color: white; font-size: 15px; text-align: center">+62
                        362 25471</p>

                </div>
            </div>
            <div class="container-fluid full-height center-footer">
                <a href="https://redsystem.id/" class="font-footer">&copy; Red
                    System <?= date('Y') ?></a>
            </div>
        </div>
    </div>
</footer>


<!--begin::Global Theme Bundle -->
<script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/custom/js/front.js" type="text/javascript"></script>


</body>
</html>