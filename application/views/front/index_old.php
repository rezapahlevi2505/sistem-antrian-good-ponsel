<html lang="en"
      class="wf-poppins-n3-active wf-poppins-n4-active wf-poppins-n5-active wf-poppins-n6-active wf-poppins-n7-active wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n5-active wf-roboto-n6-active wf-roboto-n7-active wf-active">
<!-- begin::Head -->
<head>
    <meta charset="utf-8">
    <title>Sistem Antrian SIM Online</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <link rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,400,500,600,700"
          media="all">
    <script>
        WebFont.load({
            google: {"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]},
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-grid.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap-reboot.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>assets/custom/css/front.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <link rel="shortcut icon"
          href="<?= base_url() ?>assets/polres/POLANTAS.png">
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"
      style="">
<nav class="navbar navbar-dark bg-dark">
    <div class="container ">
        <a class="navbar-brand center-desktop col-lg-12" href="#">ANTRIAN SIM</a>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01"
            aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample01">
        <ul class="navbar-nav mr-auto" style="text-align: right">
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('/proweb') ?>">Login</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm" style="background-color: dimgray">

            <!--            <div class="container-fluid" >-->
            <div class="row">
                <input value="<?= base_url() ?>/assets/polres/video/abang-jago.mp4" id="video-1" type="hidden">
                <input value="<?= base_url() ?>/assets/polres/video/sesal.mp4" id="video-2" type="hidden">
                <input value="<?= base_url() ?>/assets/polres/video/masker.mp4" id="video-3" type="hidden">
                <input value="<?= base_url() ?>/assets/polres/video/mudik.mp4" id="video-4" type="hidden">
                <input value="<?= base_url() ?>/assets/polres/video/taksu.mp4" id="video-5" type="hidden">
                <input value="<?= base_url() ?>/assets/polres/video/tertib.mp4" id="video-6" type="hidden">
                <div class="container-fluid none-mobile">
                    <video id="myVideo" width="100%" controls>
                        <source id="video-now" src="<?= base_url() ?>/assets/polres/video/abang-jago.mp4" type="video/mp4"
                                data-urutan="1">
                        Your browser does not support HTML video.
                    </video>
                </div>
                <div id="carouselExampleIndicators" class="carousel slide none-desktop" data-ride="carousel"
                     style="background-color: white;">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="gambar rounded" src="<?= base_url() ?>/assets/polres/slider-0.png"
                                 alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="gambar rounded" src="<?= base_url() ?>/assets/polres/slider-1.png"
                                 alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="gambar rounded" src="<?= base_url() ?>/assets/polres/slider-2.png"
                                 alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="container-fluid none-mobile">
                    <div class="container">
                        <br>
                        <p class="lokasi"
                           style="font-family: Roboto; color: white; font-size: 25px; text-align: center">LOKASI</p>
                        <div class="container-fluid col-lg-10">
                            <p class="lokasi"
                               style="font-family: Roboto; color: white; font-size: 20px; text-align: center">Jalan
                                Surapati No.122 Singaraja Buleleng - Bali</p>

                        </div>
                        <br>
                        <p class="telepon"
                           style="font-family: Roboto; color: white; font-size: 25px; text-align: center">TELEPON</p>
                        <div class="container-fluid col-lg-10">
                            <p class="lokasi"
                               style="font-family: Roboto; color: white; font-size: 20px; text-align: center">+62 362
                                25471</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm" style="background-color: dodgerblue">
            <div class="row">
                <div class="container-fluid">
                    <input type="hidden" id="url" value="<?= base_url('/now') ?>">
                    <input type="hidden" value="<?= $antrian->id ?>" id="id_antrian">
                    <input type="hidden" value="<?= $antrian->status ?>" id="status_antrian">
                    <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center">
                        <div class="inner">
                            <p class="teks-atas" style="color: white">Nomor Antrian SIM Saat Ini</p>
                            <p id="nomor_antrian" class="nomor-antrian margin-atas"
                               style="color: white"><?= $antrian->nomor ?></p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid" style=" background-color: #0a6aa1">

                    <div class="kt-portlet kt-portlet--height-fluid text-mobile" style="text-align: center;">
                        <div class="inner">
                            <marquee class="teks-bawah" style="color: white">Pemohon Wajib Hadir 10 Nomor Antrian
                                Sebelumnya
                            </marquee>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<footer class="footer-desktop" style=" background-color: #37373a; ">
    <div class="container-fluid full-height">

        <div class="row full-height" style="text-align: center;">
            <div class="container-fluid none-desktop" style="background-color: dimgray;">
                <br>
                <p class="lokasi"
                   style="font-family: Roboto; color: white; font-size: 20px; text-align: center; margin-bottom: 0">
                    LOKASI</p>
                <div class="container-fluid" style="width: 70% !important;">
                    <p class="lokasi" style="font-family: Roboto; color: white; font-size: 15px; text-align: center">
                        Jalan Surapati No.122 Singaraja Buleleng - Bali</p>
                </div>
                <br>
                <p class="telepon"
                   style="font-family: Roboto; color: white; font-size: 20px; text-align: center; margin-bottom: 0">
                    TELEPON</p>
                <div class="container-fluid col-lg-10">
                    <p class="lokasi" style="font-family: Roboto; color: white; font-size: 15px; text-align: center">+62
                        362 25471</p>

                </div>
            </div>
            <div class="container-fluid full-height center-footer">
                <a href="https://redsystem.id/" class="font-footer">&copy; Red
                    System <?= date('Y') ?></a>
            </div>
        </div>
    </div>
</footer>


<!-- begin::Quick Nav -->

<!--<script>-->
<!--    var aud = document.getElementById("myVideo");-->
<!--    aud.onended = function () {-->
<!--        alert("The audio has ended");-->
<!--    };-->
<!--</script>-->

<!--begin::Global Theme Bundle -->
<script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/custom/js/front.js" type="text/javascript"></script>


</body>
</html>