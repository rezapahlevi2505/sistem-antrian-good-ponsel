<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Antrian Good Ponsel</title>

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@100..900&display=swap" rel="stylesheet">
        <link href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url() ?>assets/custom/css/front.css" rel="stylesheet" type="text/css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/custom/js/front.js" type="text/javascript"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container px-5">
                <a class="navbar-brand" href="#!">
                    <img src="<?= base_url() ?>assets/good-ponsel/image/logo-putih.png" alt="Logo" style="height: 40px; width: auto;">
                </a>
                <div class="d-flex justify-content-end bg-dark text-white p-2">
                    <div id="clock"></div>
                </div>
            </div>
        </nav>      
        <div class="content">
            <div class="container">
                <div class="row text-center text-white">
                    <div class="col-lg-8 mx-auto">
                        <h1 class="display-3 "><strong>GOOD PHONE FOR EVERYONE</strong></h1>
                        <p id="greeting" class="mb-5" style="font-size: 28px;"></p>
                    </div>
                </div>
            </div>

            <div class="container">
                <input type="hidden" id="url" value="<?= base_url('/get-all') ?>">
                <div class="row text-center" id="row-queue">
                <?php foreach ($antrian as $key => $antri) { ?>
                    <?php ?>
                    <div class="col-md-4 mb-4 col-queue">
                        <div class="bg-white shadow-sm pb-5 pt-4 px-4">
                            <h1 class="my-0 display-1" id="nomor-<?= $antri->id_jasa ?>"><strong><?= $antri->nomor_label ?></strong></h1><span class="text-uppercase text-muted"><?= $antri->nama_loket ?></span>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>

        <!-- Footer-->
        <footer class="py-3 bg-dark">
            <div class="container px-4 px-lg-5"><p class="m-0 text-center text-white">Made With &#10084; Good Ponsel <?= date('Y') ?></p></div>
        </footer>

    </body>
</html>

