<?php

class Main
{

    private $ci;
    private $web_name = 'Good Ponsel';
    private $web_url = 'redsystem.id';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 30;
    private $currency = 'Rp.';

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function web_url()
    {
        return $this->web_url;
    }

    function credit()
    {
        return 'development by <a href="https://www.redsystem.id">Red System</a>';
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function currency($nominal)
    {
        return 'Rp. ' . number_format($nominal);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
//				/unlink($this->path_images . $filename);
            }
        }
    }

    function data_main()
    {
        $data = array(
            'web_name' => $this->web_name,
            'name' => $this->ci->session->userdata('name'),
            'akses_level' => $this->ci->session->userdata('akses_level'),
            'menu_list' => $this->menu_list(),
            // 'nama_loket' => $this->ci->session->userdata('nama_loket'),
            'nama_jasa' => $this->ci->session->userdata('nama_jasa')
        );

        return $data;
    }

    function id_loket(){
        return $this->ci->session->userdata('id_loket');
    }

    function id_jasa() {
        return $this->ci->session->userdata('id_jasa');
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('proweb');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            if($this->ci->session->userdata('akses_level') == 1) {
                redirect('proweb/antrian');
            } else {
                redirect('proweb/cetak'); 
            }
        }
    }
    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '?', '!', ':');
        $replace = array('-', '-', 'and', '-', '-', '-', '', '', '', '', '');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }
    function str_encrypt($string)
    {
        $ciphering = "AES-128-CTR";
        $encryption_iv = '1234567891011121';
        $encryption_key = "RedEcommerce";
        $options = 0;
        $encryption = openssl_encrypt($string, $ciphering,
            $encryption_key, $options, $encryption_iv);

        return $encryption;
    }
    function str_decrypt($string)
    {
        $ciphering = "AES-128-CTR";
        $decryption_iv = '1234567891011121';
        $decryption_key = "RedEcommerce";
        $options = 0;
        $decryption = openssl_decrypt($string, $ciphering,
            $decryption_key, $options, $decryption_iv);

        return $decryption;
    }

    function random($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function mailer_auth($subject, $to_email, $to_name, $body, $file = '')
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "smtp.gmail.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "sim.polresbuleleng@gmail.com"; //user email
            $mail->Password = "sim1234567890"; //password email
            $mail->SetFrom("sim.polresbuleleng@gmail.com ", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            if ($file) {
                $mail->addAttachment("upload/images/" . $file);
            }

            if (!$mail->Send()) {
                $data = array(
                    'success' => false,
                    'error' => $mail->ErrorInfo,
                );
            } else {
                $data = array(
                    'success' => true,
                    // 'error' => $mail->ErrorInfo,
                );
            }
            return (object)$data;
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            return $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            return $e->getMessage(); //Boring error messages from anything else!
        }
    }
    function menu_list()
    {
        $menu = array(
            'MENU' => array(
                'antrian' => array(
                    'label' => 'Antrian',
                    'route' => base_url('proweb/antrian'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 1,
                    'sub_menu' => array()
                ),
                'cetak' => array(
                    'label' => 'Cetak Antrian',
                    'route' => base_url('proweb/cetak'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 0,
                    'sub_menu' => array()
                ),
                'ulang' => array(
                    'label' => 'Cetak & Panggil Ulang',
                    'route' => base_url('proweb/ulang'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 1,
                    'sub_menu' => array()
                ),
                'admin' => array(
                    'label' => 'Admin',
                    'route' => base_url('proweb/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 1,
                    'sub_menu' => array()
                ),
                'jasa' => array(
                    'label' => 'Jasa',
                    'route' => base_url('proweb/jasa'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 1,
                    'sub_menu' => array()
                ),
                'loket' => array(
                    'label' => 'Loket',
                    'route' => base_url('proweb/loket'),
                    'icon' => 'fab fa-asymmetrik',
                    'level' => 1,
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
