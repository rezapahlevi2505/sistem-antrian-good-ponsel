<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loket extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();
        $data['admin'] = $this->m_admin->get_data()->result();
        $data['loket'] = $this->m_loket->get_data()->result();
        $data['jasa'] = $this->m_jasa->get_data()->result();
        $this->template->set('loket', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Jasa');
        $this->template->load_admin('loket/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_loket', 'Nama Loket', 'required');
        $this->form_validation->set_rules('id_jasa', 'Jasa', 'required');
        $this->form_validation->set_rules('nama_file', 'Nama File', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'nama_loket' => form_error('nama_loket'),
                    'id_jasa' => form_error('id_jasa'),
                    'nama_file' => form_error('nama_file'),
                )
            ));
        } else {
            $nama_loket = $this->input->post('nama_loket');
            $id_jasa = $this->input->post('id_jasa');
            $nama_file = $this->input->post('nama_file');

            $data = array(
                'nama_loket' => $nama_loket,
                'id_jasa' => $id_jasa,
                'nama_file' => $nama_file,
            );
            $this->m_jasa->input_data($data, 'tb_loket');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput'
            ));
        }
    }

    public function delete($id)
    {
        $where = array('id' => $id);
        $this->m_loket->delete_data($where, 'tb_loket');
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama_loket', 'Nama Loket', 'required');
        $this->form_validation->set_rules('id_jasa', 'Jasa', 'required');
        $this->form_validation->set_rules('nama_file', 'Nama File', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'nama_loket' => form_error('nama_loket'),
                    'id_jasa' => form_error('id_jasa'),
                    'nama_file' => form_error('nama_file'),
                )
            ));
        } else {
            $nama_loket = $this->input->post('nama_loket');
            $id_jasa = $this->input->post('id_jasa');
            $id = $this->input->post('id');
            $nama_file = $this->input->post('nama_file');

            $data = array(
                'nama_loket' => $nama_loket,
                'id_jasa' => $id_jasa,
                'nama_file' => $nama_file,
            );

            $where = array(
                'id' => $id
            );

            $this->m_loket->update_data($where, $data, 'tb_loket');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil di edit'
            ));

        }
    }
}
