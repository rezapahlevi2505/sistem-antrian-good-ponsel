<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ulang extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index(){
        $id_jasa = $this->main->id_jasa();
        // $jasa = $this->m_jasa->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();
        $jasa = $this->db
                ->select('tb_jasa.*, tb_loket.nama_file')
                ->join('tb_loket', 'tb_loket.id_jasa = tb_jasa.id', 'left')
                ->where('tb_jasa.id', $id_jasa)
                ->get('tb_jasa')
                ->row();
                
        $js = array(
            0 => 'ulang.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['jasa'] = $jasa;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('ulang', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('ulang/index', $data);
    }
}
