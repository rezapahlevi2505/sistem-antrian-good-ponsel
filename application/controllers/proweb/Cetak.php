<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();

    }

    public function index() {
        $js = array(
            1 => 'cetak.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('cetak', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('cetak/index', $data);
    }

    public function print($id){
        $id_jasa = $this->main->id_jasa();
        $jasa = $this->m_jasa->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();
        $antrian = $this->db
            ->select('tb_antrian.*, tb_loket.nama_loket')
            ->join('tb_loket', 'tb_antrian.id_loket = tb_loket.id', 'left')
            ->where('tb_antrian.id', $id)
            ->get('tb_antrian')
            ->row();

        $data = array(
            'antrian' => $antrian,
            'jasa' => $jasa
        );
        $this->load->view('admins/cetak/print_tiket', $data);
    }

    public function reset(){
        $id_jasa = $this->main->id_jasa();
        $jasa = $this->m_loket->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();

        $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $jasa->id_jasa)->delete('tb_antrian');
    }
}
