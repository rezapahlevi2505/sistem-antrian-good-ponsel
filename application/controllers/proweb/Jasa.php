<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jasa extends CI_Controller
{

    public function __construct()
    {
        parent:: __construct();
        $this->load->model('m_admin');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {
        $data = $this->main->data_main();
        $data['admin'] = $this->m_admin->get_data()->result();
        $data['jasa'] = $this->m_jasa->get_data()->result();
        $this->template->set('jasa', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'management Jasa');
        $this->template->load_admin('jasa/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_jasa', 'Kode Jasa', 'required');
        $this->form_validation->set_rules('nama_jasa', 'Nama Jasa', 'required');
        $this->form_validation->set_rules('color', 'Color', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'kode_jasa' => form_error('kode_jasa'),
                    'nama_jasa' => form_error('nama_jasa'),
                    'color' => form_error('color'),
                    'keterangan' => form_error('keterangan'),
                )
            ));
        } else {
            $kode_jasa = $this->input->post('kode_jasa');
            $kode_jasa = strtoupper($kode_jasa);
            $nama_jasa = $this->input->post('nama_jasa');
            $color = $this->input->post('color');
            $keterangan = $this->input->post('keterangan');

            $count_kode = $this->db->where('kode_jasa', $kode_jasa)->count_all_results('tb_jasa');
            $count_nama = $this->db->where('nama_jasa', $nama_jasa)->count_all_results('tb_jasa');

            if ($count_kode > 0) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Kode Sudah Digunakan',
                    'errors' => array(
                        'kode_jasa' => 'Kode Sudah Digunakan',
                    )
                ));
                exit;
            } elseif ($count_nama > 0) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Nama Sudah Digunakan',
                    'errors' => array(
                        'nama_jasa' => 'Nama Sudah Digunakan',
                    )
                ));

                exit;
            }


            $data = array(
                'kode_jasa' => $kode_jasa,
                'nama_jasa' => $nama_jasa,
                'color' => $color,
                'keterangan' => $keterangan,
            );
            $this->m_jasa->input_data($data, 'tb_jasa');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput'
            ));
        }
    }

    public function delete($id)
    {
        $where = array('id' => $id);
        $this->m_jasa->delete_data($where, 'tb_jasa');
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kode_jasa', 'Kode Jasa', 'required');
        $this->form_validation->set_rules('nama_jasa', 'Nama Jasa', 'required');
        $this->form_validation->set_rules('color', 'Color', 'required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'kode_jasa' => form_error('kode_jasa'),
                    'nama_jasa' => form_error('nama_jasa'),
                    'color' => form_error('color'),
                    'keterangan' => form_error('keterangan'),
                )
            ));
        } else {
            $kode_jasa = $this->input->post('kode_jasa');
            $kode_jasa = strtoupper($kode_jasa);
            $nama_jasa = $this->input->post('nama_jasa');
            $color = $this->input->post('color');
            $keterangan = $this->input->post('keterangan');
            $id = $this->input->post('id');

            $count_kode = $this->db->where('id !=', $id)->where('kode_jasa', $kode_jasa)->count_all_results('tb_jasa');
            $count_nama = $this->db->where('id !=', $id)->where('nama_jasa', $nama_jasa)->count_all_results('tb_jasa');

            if ($count_kode > 0) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Kode Sudah Digunakan',
                    'errors' => array(
                        'kode_jasa' => 'Kode Sudah Digunakan',
                    )
                ));
                exit;
            } elseif ($count_nama > 0) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Nama Sudah Digunakan',
                    'errors' => array(
                        'nama_jasa' => 'Nama Sudah Digunakan',
                    )
                ));

                exit;
            }


            $data = array(
                'kode_jasa' => $kode_jasa,
                'nama_jasa' => $nama_jasa,
                'color' => $color,
                'keterangan' => $keterangan,
            );

            $where = array(
                'id' => $id
            );

            $this->m_jasa->update_data($where, $data, 'tb_jasa');
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil di edit'
            ));

        }
    }
}
