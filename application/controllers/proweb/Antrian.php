<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends CI_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('m_admin');
        $this->load->model('m_loket');
        $this->load->model('m_jasa');
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index(){

        $id_jasa = $this->main->id_jasa();
        // $jasa = $this->m_jasa->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();
        $jasa = $this->db
                ->select('tb_jasa.*, tb_loket.nama_file')
                ->join('tb_loket', 'tb_loket.id_jasa = tb_jasa.id', 'left')
                ->where('tb_jasa.id', $id_jasa)
                ->get('tb_jasa')
                ->row();
        $js = array(
            0 => 'custom.js'
        );

        $css = array(
            0 => 'custom.css'
        );

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();

        if (empty($nomor)) {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            if (empty($nomor)){
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil',
                    'id_jasa' => $id_jasa,
                    'status_pengerjaan' => 'finish'
                );

                $this->db->insert('tb_antrian', $data);

                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->where_in('status', array('sudah_dipanggil','lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            }
        } else {
            $kondisi = 'ada';
        }

        $terakhir = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->order_by('nomor', 'desc')->get('tb_antrian')->row();

        if (empty($terakhir)){
            $terakhir = array(
                'id' => null,
                'nomor' => 'Tidak Ada Antrian',
                'nomor_label' => 'Tidak Ada Antrian'
            );
        }

        $data = $this->main->data_main();
        $data['js'] = $js;
        $data['css'] = $css;
        $data['antrian'] = $nomor;
        $data['terakhir'] = $terakhir;
        // $data['loket'] = $loket;
        $data['jasa'] = $jasa;
        $data['admin'] = $this->m_admin->get_data()->result();
        $this->template->set('antrian', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Antrian');
        $this->template->load_admin('antrian/index', $data);
    }
}
