<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_login');
		$this->load->model('m_admin');
		$this->load->model('m_jasa');
		$this->load->library('main');

		$this->main->check_login();
	}

	public function index()
	{
		$data = $this->main->data_main();
		$data['jasa'] = $this->m_jasa->get_data()->result();
		$this->load->view('admins/login/index', $data);
	}

	public function forgot(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required|callback_check_username');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'username' => form_error('username'),
                )
            ));
        } else {
            echo json_encode(array(
                'status' => 'success',
                'message' => 'Permintaan Anda Sedang Diproses',
            ));
        }
    }

    function check_username() {
        $this->load->library('form_validation');

        $username = $this->input->post('username', true);
        $id_jasa = $this->input->post('id_jasa', true);

        if (!$id_jasa){
            $username = null;
        }
        $where = array(
            'username' => $username
        );
        $cek = $this->m_login->cek_login("tb_admin", $where)->num_rows();
        if ($cek > 0) {

            $user = $this->db->where($where)->get('tb_admin')->row();

            $random = $this->main->random();
            $this->db->where('id', $user->id)->update('tb_admin', array('code' => $random));

            $body = $this->load->view('admins/login/email', array('random' => $random), TRUE);

            $this->main->mailer_auth('Pemulihan Password', $user->email, $user->name, $body);

            return TRUE;

        } else {
            $this->form_validation->set_message('check_username', 'Username Tidak Ditemukan');
            return FALSE;
        }
    }

	function check_akun() {
		$this->load->library('form_validation');

		$username = $this->input->post('username', true);
		$id_jasa = $this->input->post('id_jasa', true);
		$password = md5($this->input->post('password', true));
		$where = array(
			'username' => $username,
			'password' => $password
		);
		$cek = $this->m_login->cek_login("tb_admin", $where)->num_rows();
		if ($cek > 0 && $id_jasa) {
			$user = $this->db->get_where('tb_admin', $where)->row();

            $nama_jasa = $this->db->where('id', $id_jasa)->get('tb_jasa')->row()->nama_jasa;

			$data_session = array(
				'username' => $user->username,
				'name' => $user->name,
				'status' => "login",
                'id_jasa' => $id_jasa,
                'akses_level' => $user->akses_level,
                'nama_jasa' => $nama_jasa
			);

			$this->session->set_userdata($data_session);

			return TRUE;

		} else {
			$this->form_validation->set_message('check_akun', 'Username atau Password Salah');
			return FALSE;
		}
	}

	public function process()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|callback_check_akun');
        $this->form_validation->set_rules('id_jasa', 'Jasa', 'required');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Isi form belum benar',
				'errors' => array(
					'username' => form_error('username'),
					'password' => form_error('password'),
					'id_jasa' => form_error('id_jasa'),
				)
			));
		} else {
			echo json_encode(array(
				'status' => 'success'
			));
		}

	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

	function recovery($data){

        $cek = $this->m_login->cek_login("tb_admin", array('code' => $data))->num_rows();
        $this->load->view('admins/login/reset', array('code' => $data, 'cek' => $cek));

    }

    function simpan_password(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password1', 'Password', 'required');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|matches[password1]');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'password1' => form_error('password1'),
                    'password2' => form_error('password2'),
                )
            ));
        } else {
            $code = $this->input->post('code');
            $password1 = md5($this->input->post('password1'));
            $password2 = md5($this->input->post('password2'));

            $this->load->model('m_admin');
            if ($password1 == $password2) {
                $data = array(
                    'code' => null,
                    'password' => $password1,
                );

                $where = array(
                    'code' => $code
                );

                $this->m_admin->update_data($where, $data, 'tb_admin');
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'data berhasil disimpan',
                    'url' => base_url('/proweb')
                ));
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'Isi form belum benar',
                    'errors' => array(
                        'password1' => 'Password belum sesuai'
                    )
                ));
            }
        }
    }
}
