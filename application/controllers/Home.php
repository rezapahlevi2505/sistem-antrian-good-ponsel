<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
        $this->load->model('m_loket');
    }

    public function index()
    {
        $data_return = $this->get_semua();

        $data = array(
            'antrian' => $data_return
        );
        return $this->load->view('front/index_front', $data);
    }

    public function get_semua()
    {
        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d')
        );

        $aktif = array(
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $data_antrian = $this->db->where('tanggal', date('Y-m-d'))->order_by('nomor')->get('tb_antrian')->result();

        $id_jasa = array();

        if (!empty($data_antrian)) {
            foreach ($data_antrian as $key => $value) {
                if ($value->id_jasa) {
                    $id_jasa[$key] = $value->id_jasa;
                }
            }
            $jasa = $this->db->where_not_in('id', $id_jasa)->get('tb_jasa')->result();
        } else {
            $jasa = $this->db->get('tb_jasa')->result();
        }

        if (!empty($jasa)) {
            foreach ($jasa as $l) {
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil',
                    'id_jasa' => $l->id,
                );

                $this->db->insert('tb_antrian', $data);
            }
        }

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d')
        );

        $data_nomor = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_antrian.id_loket, tb_antrian.id_jasa, max(nomor) as nomor,  CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
            ->from('tb_antrian')
            ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
            ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
            ->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
            ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->result();
            
        $data_return = array();
        $no = 0;
        $data_loket_ada = array();

        if (empty($data_nomor)) {
            $data_return = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_antrian.id_loket, tb_antrian.id_jasa, max(nomor) as nomor,  CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
                    ->from('tb_antrian')
                    ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
                    ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
                    ->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
                    ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->result();
        } else {

            foreach ($data_nomor as $k => $n) {
                if ($n->id_jasa) {
                    $data_jasa_ada[$k] = $n->id_jasa;
                    $data_return[$n->id_jasa] = $n;
                }
            }

            if (!empty($data_jasa_ada)) {
                $data_jasa_kosong = $this->db->where_not_in('id', $data_jasa_ada)->get('tb_jasa')->result();

                foreach ($data_jasa_kosong as $l) {

                    $nomor = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_loket.id as id_loket, tb_antrian.id_jasa, max(nomor) as nomor, CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
                        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
                        ->from('tb_antrian')
                        ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
                        ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
                        ->where('tanggal', date('Y-m-d'))
                        ->where('id_loket', $l->id)
                        ->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
                        ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->row();

                    $no = $nomor->id_jasa;
                    $data_return[$no] = json_encode($nomor);
                }

            }

        }

        $data_antrian = ksort($data_return);


        return $data_return;
    }

    public function next()
    {
        $id_jasa = $this->main->id_jasa();
        $data = $this->input->post();

        $insert = array(
            'status' => 'sudah_dipanggil',
            'status_pengerjaan' => 'finish'
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa

        );

        $where = array(
            'status' => 'belum_dipanggil',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $where_aktif = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $kondisi = 'kosong';

        if ($data['id']) {
            if ($data['status'] == 'aktif') {
                $this->db->set($insert)->where('id', $data['id'])->update('tb_antrian');
            }

            $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();


            if (empty($nomor)) {
                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();
            } else {
                $this->db->set(array('status' => 'aktif'))->where('id', $nomor->id)->update('tb_antrian');
                $nomor = $this->db->where($where_aktif)->get('tb_antrian')->row();
            }


        } else {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();
        }

        echo json_encode(array(
            'status' => 'success',
            'nomor' => $nomor,
        ));
    }

    public function skip()
    {
        $id_jasa = $this->main->id_jasa();
        $data = $this->input->post();

        $insert = array(
            'status' => 'lewat'
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $where = array(
            'status' => 'belum_dipanggil',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $where_aktif = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        if ($data['id']) {
            if ($data['status'] == 'aktif') {
                $this->db->set($insert)->where('id', $data['id'])->update('tb_antrian');
            }

            $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();


            if (empty($nomor)) {
                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();
            } else {
                $this->db->set(array('status' => 'aktif'))->where('id', $nomor->id)->update('tb_antrian');
                $nomor = $this->db->where($where_aktif)->get('tb_antrian')->row();
            }


        } else {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();
        }

        echo json_encode(array(
            'status' => 'success',
            'nomor' => $nomor
        ));
    }

    function tambah_nomor()
    {
        $id_jasa = $this->main->id_jasa();
        $jasa = $this->m_loket->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();

        $id_loket = $this->get_available_loket_id($id_jasa);

        $count = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->count_all_results('tb_antrian');

        $data = array(
            'nomor' => $count,
            'nomor_label' => $jasa->kode_jasa . $count,
            'tanggal' => date('Y-m-d'),
            'status' => 'belum_dipanggil',
            'id_loket' => $id_loket,
            'id_jasa' => $id_jasa,
            'status_pengerjaan' => 'on progress'
        );

        $this->db->insert('tb_antrian', $data);

        $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->order_by('nomor', 'desc')->get('tb_antrian')->row();

        echo json_encode(array(
            'status' => 'success',
            'nomor' => $nomor
        ));
    }

    public function now()
    {
        $id_jasa = $this->main->id_jasa();

        $loket = $this->m_loket->get_data_filter(array('tb_jasa.id' => $id_jasa))->row();

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa
        );

        $aktif = array(
            'status' => 'sudah_dipanggil',
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian')->row();

        if (empty($nomor)) {
            $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            if (empty($nomor)) {
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'id_jasa' => $id_jasa,
                    'id_jasa' => $loket->id_jasa,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil'
                );

                $this->db->insert('tb_antrian', $data);

                $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->where_in('status', array('sudah_dipanggil', 'lewat'))->order_by('nomor', 'desc')->get('tb_antrian')->row();

            }
        } else {
            $kondisi = 'ada';
        }


        echo json_encode(array(
            'status' => 'success',
            'nomor' => $nomor,
            'kondisi' => $kondisi
        ));
    }

    public function last()
    {
        $id_jasa = $this->main->id_jasa();
        $nomor = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->order_by('nomor', 'desc')->get('tb_antrian')->row();

        if (empty($nomor)) {
            $nomor = array(
                'id' => null,
                'nomor' => 0,
                'nomor_label'
            );
        }

        echo json_encode(array(
            'status' => 'success',
            'nomor' => $nomor
        ));
    }

    public function selanjutnya()
    {
        $id_loket = $this->main->id_loket();
        $where = array(
            'status' => 'belum_dipanggil',
            'tanggal' => date('Y-m-d'),
            'id_loket' => $id_loket
        );
        $nomor = $this->db->where($where)->order_by('nomor')->get('tb_antrian', 2)->result();

        if (empty($nomor[0])) {
            $antrian = array(
                'id' => null,
                'nomor' => 'Tidak Ada Antrian'
            );
        } else {
            $antrian = $nomor[1];
        }

        echo json_encode(array(
            'status' => 'success',
            'nomor' => $antrian
        ));
    }

    public function semua()
    {
        $id_jasa = $this->main->id_jasa();
        $search = $this->input->get('search', true);
        $where = array(
            'tanggal' => date('Y-m-d'),
            'id_jasa' => $id_jasa,
        );

        $nomor = $this->db->where($where)->where_not_in('nomor', array(0));
        if ($search) {
            $nomor = $this->db->where('nomor', $search);
        }
        $nomor = $this->db->order_by('nomor')->get('tb_antrian')->result();

        if (!empty($nomor)) {
            foreach ($nomor as $row) {
                $data[] = array("id" => json_encode($row), "text" => $row->nomor_label);
            }
            echo json_encode($data);
        } else {
            echo "Hasil Kosong";
        }
    }

    function get_semua_api()
    {
        $data = array();
        $str = base_url();
        $str = preg_replace('#^https?://#', '', rtrim($str, '/'));
        $data['url'] = $str;
        $data['antrian'] = $this->get_all_api();

        echo json_encode($data);
    }

    function tambah_nomor_api()
    {
        $id_jasa = $this->input->get('id_jasa');

        $jasa = $this->db->where('id', $id_jasa)->get('tb_jasa')->row();
        $loket = $this->db->where('id_jasa', $id_jasa)->get('tb_loket')->row();
        $semua_nomor = $this->get_all_api();
        $count = $this->db->where('tanggal', date('Y-m-d'))->where('id_jasa', $id_jasa)->count_all_results('tb_antrian');
        $data = array(
            'nomor' => $count,
            'nomor_label' => $jasa->kode_jasa . $count,
            'tanggal' => date('Y-m-d'),
            'status' => 'belum_dipanggil',
            'id_loket' => $loket->id,
            'id_jasa' => $id_jasa,
        );
        $this->db->insert('tb_antrian', $data);

        $nomor = $this->db->select('tb_antrian.*, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_loket.nama_loket')->from('tb_antrian')
            ->join('tb_loket', 'tb_loket.id = tb_antrian.id_loket', 'left')
            ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa', 'left')
            ->where('tanggal', date('Y-m-d'))->where('tb_antrian.id_jasa', $id_jasa)->order_by('nomor', 'desc')->get()->row();

        $semua_nomor = $this->get_all_api();

        $str = base_url();
        $str = preg_replace('#^https?://#', '', rtrim($str, '/'));
        echo json_encode(array(
            'status' => 'success',
            'url' => $str,
            'nomor' => $nomor,
            'antrian' => $semua_nomor,
        ));

    }

    public function get_all_api()
    {
        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d')
        );

        $aktif = array(
            'tanggal' => date('Y-m-d')
        );

        $kondisi = 'kosong';

        $data_antrian = $this->db->select('id_jasa')->where('tanggal', date('Y-m-d'))->order_by('nomor')->get('tb_antrian')->result();

        $id_jasa = array();

        if (!empty($data_antrian)) {
            foreach ($data_antrian as $key => $value) {
                if ($value->id_jasa) {
                    $id_jasa[$key] = $value->id_jasa;
                }
            }
            $jasa = $this->db->where_not_in('id', $id_jasa)->get('tb_jasa')->result();
        } else {
            $jasa = $this->db->get('tb_jasa')->result();
        }

        if (!empty($jasa)) {
            foreach ($jasa as $l) {
                $data = array(
                    'nomor' => 0,
                    'nomor_label' => 0,
                    'tanggal' => date('Y-m-d'),
                    'status' => 'sudah_dipanggil',
                    'id_jasa' => $l->id,
                );

                $this->db->insert('tb_antrian', $data);
            }
        }

        $where = array(
            'status' => 'aktif',
            'tanggal' => date('Y-m-d')
        );

        $data_nomor = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_antrian.id_loket, tb_antrian.id_jasa, max(nomor) as nomor,  CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
            ->from('tb_antrian')
            ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
            ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
            ->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
            ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->result();

        $data_return = array();
        $no = 0;
        $data_loket_ada = array();

        if (empty($data_nomor)) {
            $data_return = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_antrian.id_loket, tb_antrian.id_jasa, max(nomor) as nomor,  CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
                    ->from('tb_antrian')
                    ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
                    ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
                    ->where('tanggal', date('Y-m-d'))->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
                    ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->result();
        } else {

            foreach ($data_nomor as $k => $n) {
                if ($n->id_jasa) {
                    $data_jasa_ada[$k] = $n->id_jasa;
                    $data_return[$n->id_jasa] = $n;
                }
            }

            if (!empty($data_jasa_ada)) {
                $data_jasa_kosong = $this->db->where_not_in('id', $data_jasa_ada)->get('tb_jasa')->result();

                foreach ($data_jasa_kosong as $l) {

                    $nomor = $this->db->select('tb_antrian.id, tb_antrian.tanggal, tb_loket.id as id_loket, tb_antrian.id_jasa, max(nomor) as nomor, CONCAT(tb_jasa.kode_jasa, max(tb_antrian.nomor)) as nomor_label, 
                        tb_loket.nama_loket, tb_jasa.nama_jasa, tb_jasa.kode_jasa, tb_jasa.color')
                        ->from('tb_antrian')
                        ->join('tb_jasa', 'tb_jasa.id = tb_antrian.id_jasa')
                        ->join('tb_loket', 'tb_jasa.id = tb_loket.id_jasa')
                        ->where('tanggal', date('Y-m-d'))
                        ->where('id_loket', $l->id)
                        ->where_in('status', array('sudah_dipanggil', 'lewat', 'aktif'))
                        ->order_by('nomor', 'desc')->group_by('id_jasa')->get()->row();

                    $no = $nomor->id_jasa;
                    $data_return[$no] = json_encode($nomor);
                }

            }

        }

        $data_antrian = ksort($data_return);

        $data_kirim = array();

        foreach ($data_return as $key => $value) {
            array_push($data_kirim, $data_return[$key]);
        }


        return $data_kirim;
    }

    function get_available_loket_id($id) {
        // Fetch all loket data
        $loket_all = $this->db->where('id_jasa', $id)->get('tb_loket')->result_array();
    
        // Count the number of "on progress" for each loket
        $progress_counts = [];
        foreach ($loket_all as $row) {
            $count = $this->db->where('id_loket', $row['id'])
                            ->where('status_pengerjaan', 'on progress')
                            ->where('tanggal', date('Y-m-d'))
                            ->count_all_results('tb_antrian');
                            
            $progress_counts[$row['id']] = $count;
        }
    
        // If there are no queues or all queues are not in progress, assign id_loket to 1
        if (array_sum($progress_counts) == 0) {
            return 1;
        } else {
            // Find the id_loket with the least "on progress" queues
            $min_progress = min($progress_counts);
            $min_progress_lokets = array_keys($progress_counts, $min_progress);
    
            // If there are multiple id_lokets with the same minimal progress, pick one randomly
            return $min_progress_lokets[array_rand($min_progress_lokets)];
        }
    }
}
